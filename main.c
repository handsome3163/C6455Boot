// ======================================================
//		Copyright (C) 2013 VisCodec
//           All Rights Reserved
// ======================================================
//
//	File Name:		videosrv.c
//	Discription:	creat  task .process interrupt and others;
//								
//								
//	Author:			邓松峰  dsf323@163.com
//	Create Date:	2013/4/20
//	Last Modified:	2013/5/10
//	Modified by:	邓松峰
//
//-------------------------------------------------------------------------
#include <stdio.h>
/**************************************************/
//#pragma CODE_SECTION(flashburn,".flash_burn")




int erase_flash(char* flash_addrs);
int program_flash(char * flash_addrs, int num_words,char *src);
int poll_data(char *,unsigned char );
/**************************************main******************************/
/* Main code body.                                                      */
/************************************************************************/

///////
//The data below is decided by the map of your code
#define	INDEX_LEN		16
#define FLASH_ADDRS 		0xB0000000
#define BOOTLOAD_START 		0x00800000
#define BOOTLOAD_END 		0x00800400//0x00001000

#define	CODE_START 0x008626e0
//#define	CODE_START 0x00914ce0    

//#define	CODE_END   0x008B3CB8
//#define	CODE_END 0x8f3958
//#define	CODE_END 0x00957A54
#define	CODE_END 0x00957D24
//#define	CODE_END 0x00959e58    
//#define	CODE_END   0x008B4718//0x008B4530//0x008B42DC
//#define	CODE_START		   (0x008b3140-INDEX_LEN)// (0x008b30a0-INDEX_LEN)	//(0x008b3180-INDEX_LEN)//0xb0000200//
//#define	CODE_END		    0x8f7ae8//0x008f7278//0x008f6f64//0x008f71d4 //	0x008f67dc              //(0xb003f480+INDEX_LEN)	//	0x8003e000//
#define	DATA_START			0x00824500//0x80001000	//0x80106000	//
#define	DATA_END			0x00832000 //0x80050000	//0x80114000	//
#define BOOTLOAD_FLASHADDR	0xB0000000	//should be 0x90000000
#define HEX_FLASHADDR		0xB0001000//0xB0000000	//0x90001000 is fixed because of the bootload.asm
#define	BOOTLOAD_LEN	BOOTLOAD_END-BOOTLOAD_START
#define	CODE_LEN		CODE_END-CODE_START
#define	DATA_LEN		DATA_END-DATA_START
#define TRUE 1
#define FALSE 0

void main()
{
	int 	Store_Len,pass;
	char 	*Dst_Addr_In_FLASH,*Start_Addr_In_SDRAM;
	unsigned int Addr_Index[INDEX_LEN/sizeof(int)]={CODE_START+INDEX_LEN,CODE_LEN-INDEX_LEN,DATA_START,DATA_LEN};
//	unsigned int Addr_Index[INDEX_LEN/sizeof(int)]={0x80001000,0x50000,DATA_START,DATA_LEN};

//	memcpy((char*)0x80000000,Addr_Index,INDEX_LEN);
	//Dst_Addr_In_FLASH=(char*)FLASH_ADDRS;
   	//pass = erase_flash(Dst_Addr_In_FLASH);
	pass = erase_flash((char*)FLASH_ADDRS);

	if (pass)
   	{
		
		//make sure no overlay 
		//if(BOOTLOAD_LEN>HEX_FLASHADDR-BOOTLOAD_LEN)
		if(BOOTLOAD_LEN	>0x400)
			printf("bootload len is too large\n");
		if(CODE_LEN		>0x200000)	
			printf("the code len is too large\n");
			
		//store boodload 
	/*	Start_Addr_In_SDRAM	= (char*)BOOTLOAD_START;	
		Store_Len			= BOOTLOAD_LEN;  					
     	Dst_Addr_In_FLASH	= (char*)BOOTLOAD_FLASHADDR;*/
     	
     	if(BOOTLOAD_LEN>0)
     	{
			//pass = program_flash(Dst_Addr_In_FLASH,Store_Len,Start_Addr_In_SDRAM);
			pass = program_flash((char*)FLASH_ADDRS,BOOTLOAD_LEN,(char*)BOOTLOAD_START);
			//memcpy(Dst_Addr_In_FLASH,Start_Addr_In_SDRAM,Store_Len);
			if (!pass)
	        	printf("Failed in program operation!\n");
			else
				printf("Successful erase and program!\n");
		}
		//return ;
		//store the index			
     /*	Dst_Addr_In_FLASH	= (char*)HEX_FLASHADDR;		
		Start_Addr_In_SDRAM	= (char*)(CODE_START);	
		Store_Len			= CODE_LEN;  */
		//memcpy(Start_Addr_In_SDRAM,Addr_Index,INDEX_LEN);	
		memcpy((char*)CODE_START,Addr_Index,INDEX_LEN);					
     	//Dst_Addr_In_FLASH	+= INDEX_LEN;		
     	//Dst_Addr_In_FLASH	= (char*)CODE_FLASHADDR;
     	if(CODE_LEN>0) 
     	{
		//	pass = program_flash(Dst_Addr_In_FLASH,Store_Len,Start_Addr_In_SDRAM);
			pass = program_flash((char*)HEX_FLASHADDR,CODE_LEN,(char*)CODE_START);
			//memcpy(Dst_Addr_In_FLASH,Start_Addr_In_SDRAM,Store_Len);
			if (!pass)
	        	printf("Failed in program operation!\n");
			else
				printf("Successful erase and program!\n");
		}							            
      }
      else
        printf("Failed in erase operation!!!!\n");
      asm(" nop");  
}  

/**************************************************************************/
/* erase_flash : Routine to erase entire FLASH memory AM29LV033 (8Mx8bit) */
/* Inputs:                                                                */
/* flash_ptr: Address of the FLASH                                        */
/* Return value:                                                          */
/* Returns TRUE if passed, or FALSE if failed. Pass or failure is         */
/* determined during the poll_data routine.                               */
/*                                                                        */
/**************************************************************************/
//#pragma CODE_SECTION(erase_flash,".flash_burn")
int erase_flash(char * flash_ptr)
{
  // char * ctrl_addr1 = (char *) ((int)flash_ptr + (0x555<<3) );
  // char * ctrl_addr2 = (char *) ((int)flash_ptr + (0x2aa<<3) );
   char * ctrl_addr1 = (char *) ((int)flash_ptr + 0xaaa );
   char * ctrl_addr2 = (char *) ((int)flash_ptr + 0x555 );
   int pass = TRUE;
   * ctrl_addr1 = 0xaa; /* Erase sequence  */
   * ctrl_addr2 = 0x55;
   * ctrl_addr1 = 0x80;
   * ctrl_addr1 = 0xaa;
   * ctrl_addr2 = 0x55;
   * ctrl_addr1 = 0x10;
   pass = poll_data(flash_ptr, (unsigned char) 0xff);
   if (!pass)
      printf("Erase flash error!\n");
   else     
      printf("Erase flash ok!\n");
   return pass;
}
/****************************************************************************/
/* program_flash: Routine to program FLASH AM29LV033                        */
/* Inputs:                                                                  */
/* flash_ptr: Address of the FLASH PEROM                                    */
/* lenth : The lenth that the data to be programmed                         */
/* Return value:                                                            */
/* Returns TRUE if passed, or FALSE if failed. Pass or failure is           */
/* determined during the poll_data routine.                                 */
/*                                                                          */
/****************************************************************************/
//#pragma CODE_SECTION(program_flash,".flash_burn")
int program_flash(char * flash_ptr, int length,char *src)
{
   int i;
   int pass;
   char * ctrl_addr1 = (char *) ((int)flash_ptr + 0xAAA);
   char * ctrl_addr2 = (char *) ((int)flash_ptr + 0x555);
   for (i = 0; i < length; i++)
   {
      * ctrl_addr1 = 0xaa;
      * ctrl_addr2 = 0x55;
      * ctrl_addr1 = 0xa0;
      * flash_ptr++ = src[i];
   pass = poll_data(flash_ptr-1,src[i] );
   if (!pass)
      printf("Failed at address %x \n\n", (int) flash_ptr-1);
   }
   return pass;
}
/***************************************************************************/
/* poll_data: Routine to determine if Flash has successfully completed the */
/* program or erase algorithm. This routine will loop until                */
/* either the embedded algorithm has successfully completed or             */
/* until it has failed.                                                    */
/*                                                                         */
/* Inputs:                                                                 */
/* prog_ptr : Address just programmed                                      */
/* prog_data: Data just programmed to flash                                */
/* Return value:                                                           */
/* Returns TRUE if passed, or FALSE if failed.                             */
/*                                                                         */
/***************************************************************************/
//#pragma CODE_SECTION(poll_data,".flash_burn")
int poll_data(char * prog_ptr, unsigned char prog_data)
{
   unsigned char data;
   int fail = FALSE;
   do 
   {
      data = (unsigned char) * prog_ptr;
         if (data != (prog_data & 0Xff))                /* is D7 != Data? */
         {
            if ((data & 0x20) == 0x20)                  /*is D5 = 1 ? */
            {
               data = (unsigned char) * prog_ptr;
                  if (data != (prog_data & 0Xff))       /* is D7 = Data? */
                     fail = TRUE;
                  else
                     return TRUE;                       /* PASS */
            }
          }
          else
             return TRUE;                               /* PASS */
    } 
    while (!fail);
    return FALSE;                                       /* FAIL */
}

/****/
